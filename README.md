#LAMP stack on ubuintu 16.04 with phalcon 2.0.13
This docker compose file will provision three containers.
```bash
Creating 5.5.49-mysql ... done
Creating 5.6.38-apache2-php ... done
Creating phpmyadmin         ... done
Attaching to 5.5.49-mysql, phpmyadmin, 5.6.38-apache2-php
```
![Alt text](/img/php5onubuntu1604.png?raw=true "Main Page")

![Alt text](/img/phalcon2013.png?raw=true "Main Page")

##How to run
```bash
git clone git@bitbucket.org:nj-chandradasa/docker-compose-for-lamp.git
cd docker-compose-for-lamp
docker-compose up
```
##How to stop
```bash
cd docker-compose-for-lamp
docker-compose down
```

You may specify mysql database passwords and hosts for remote login in following section.
% indicates that database is accessble from anyhost, this will allow both phpmyadmin and apache2 containers to access database.

```yaml
environment:
      MYSQL_ROOT_PASSWORD: tiger
      DB_REMOTE_ROOT_NAME: root
      DB_REMOTE_ROOT_PASS: tiger
      DB_REMOTE_ROOT_HOST: '%'
```

If you wish to persist mysql data, you may mount a host directory to mysql /var/lib/mysql location by specifying following parameter.
```yaml
volumes:
      - ${MYSQL_DATA_DIR-./data/mysql}:/var/lib/mysql
```

When docker-compose up successfully provisions the three containers, you can access the main page by hitting http://ocalhost in your browser.

In the main page, click phpinfo() to access phpinfo page. 
![Alt text](/img/main_page.png?raw=true "Main Page")

If you access http://localhost:8080 that will take you to phpmyadmin page. Please provide the database remote credentials here to login.
![Alt text](/img/phpmyadmin.png?raw=true "Main Page")

These php pages are mounted from www directory to the apache2 container.
```python
volumes: 
      - ${DOCUMENT_ROOT-./www}:/var/www/html
      - ${PHP_INI-./config/php/php.ini}:/
      - ${VHOSTS_DIR-./config/vhosts}:/etc/apache2/sites-enabled
      - ${LOG_DIR-./logs/apache2}:/var/log/apache2
```

You can adjust ports parameter to bind host ports with respective container port.
```python
ports:
    - '8080:80'
```
